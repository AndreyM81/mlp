#!/bin/bash
git fetch --tags

VERSION=$(semver-tags --last)
if [[ -z "$VERSION" ]] 
then
echo "version not exist"
git tag -a 1.0.0 -m "Initial first tag"
else
echo "version is $VERSION"

fi

# BUILD=b.$BITBUCKET_BUILD_NUMBER
# git tag -a $BUILD -m "$BUILD" $BITBUCKET_COMMIT

# if [[ $BITBUCKET_BRANCH == "master" ]]
#     then
#     COMMIT_MESSAGE=$(git log -n 1 --pretty=format:%s $BITBUCKET_COMMIT)
#     echo $COMMIT_MESSAGE
    # LEVEL=patch
    # if [[ $COMMIT_MESSAGE == *"#major"* ]]
    #   then LEVEL=major
    #   else
    #     if [[ $COMMIT_MESSAGE == *"#minor"* ]]
    #       then LEVEL=minor
    #     fi
    # fi
    # echo "Up version as $LEVEL"
    # VERSION=$(semver -i $LEVEL $(semver-tags --last))
    # echo "Current Version: $VERSION"
    # git tag -a $VERSION -m "$VERSION" $BITBUCKET_COMMIT
    # JSON_VERSION="$VERSION-$BUILD"
#     else
#     JSON_VERSION="$BUILD"
# fi

git push origin --tags
# echo "{\"version\":\"$JSON_VERSION\"}" > frontversion.json
# echo "{\"version\":\"$JSON_VERSION\"}" > apiversion.json